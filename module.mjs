const MODULE_ID = "door-sounds";

import {registerSettings} from "./settings.mjs"

Hooks.on('setup', () => {
  game.settings.register(MODULE_ID, "collection", {
    scope: 'world',
    config: false,
    type: Object,
    default: {
      collection: []
    }
  });
  registerSettings();
});

Hooks.on('ready', async function addSounds(){
  const {collection} = game.settings.get(MODULE_ID, "collection") ?? [];
  let sounds = {};
  if(!collection) return;
  for(let sound of collection){
    sounds[sound.key] = {
      label: sound.label,
      open: sound.open,
      close: sound.close,
      test: sound.test,
      unlock: sound.unlock,
      lock: sound.lock
    };
  }
  foundry.utils.mergeObject(CONFIG.Wall.doorSounds, sounds);
});
