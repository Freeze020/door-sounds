# Door Sounds

A simple way to add your own collection of door sounds to the wall configuration UI.

Do you have your own sounds to play on door's closing, opening, locking and unlocking events?
Do you have no idea how to get those to play with the Door UI?
Look no further, this module gives you a configuration where you can set a Label and sounds belonging to each door event.

Export your settings as a JSON for an easy import in your other worlds so you dont have to recreate everything in your other worlds.

--Enjoy!

NOTE: 
The door sounds you set on your own doors can conflict with settings used in the Arms-Reach module which allowed sounds on doors before FoundryVTT did and they conflict.
So if you hear double sounds, check if you have that module running. (thanks discorduser @wyrm9426)