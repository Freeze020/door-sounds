const MODULE_ID = "door-sounds";

async function deleteSettingEntry(key) {
  let { collection } = game.settings.get(MODULE_ID, "collection");
  const index = collection.findIndex(e => e.key === key);
  if (index < 0) return;
  collection.splice(index, 1);
  await game.settings.set(MODULE_ID, "collection", { collection });
}

async function saveSettingEntry(html, key) {
  let { collection } = game.settings.get(MODULE_ID, "collection");
  const data = new FormDataExtended(html.parentNode.querySelector("FORM")).object;
  if (key === "new") {
    const soundKey = collection.length ? `sound${collection.length + 1}` : "sound1";
    collection.push({ key: soundKey, label: data.label, open: data.open, close: data.close, lock: data.lock, unlock: data.unlock, test: data.test });
    await game.settings.set(MODULE_ID, "collection", { collection })
  }
  else {
    const index = collection.findIndex(e => e.key === key);
    collection[index] = { key, label: data[`label-${key}`], open: data[`open-${key}`], close: data[`close-${key}`], lock: data[`lock-${key}`], unlock: data[`unlock-${key}`], test: data[`test-${key}`] }
    await game.settings.set(MODULE_ID, "collection", { collection })
  }
}

async function importJson() {
  new Dialog({
    title: `Import JSON`,
    content: await renderTemplate("templates/apps/import-data.html", {
      hint1: game.i18n.format("DOCUMENT.ImportDataHint1", {document: "Door Sound Setting"}),
      hint2: game.i18n.format("DOCUMENT.ImportDataHint2", {name: "your Door Sound Settings"})
    }),
    buttons: {
      import: {
        icon: '<i class="fas fa-file-import"></i>',
        label: "Import",
        callback: async (html) => {
          const form = html.find("form")[0];
          if ( !form.data.files.length ) return ui.notifications.error("You did not upload a data file!");
          const json = await readTextFromFile(form.data.files[0])
          const collection = JSON.parse(json);
          await game.settings.set(MODULE_ID, "collection", collection);
        }
      },
      no: {
        icon: '<i class="fas fa-times"></i>',
        label: "Cancel"
      }
    },
    default: "import"
  }, {
    width: 400
  }).render(true);
}

async function exportJson() {
  let collection = game.settings.get(MODULE_ID, "collection");
  const json = JSON.stringify(collection);
  await saveDataToFile(json, "application/json", "door-sounds-save-file.json");
}

class DoorSoundsSettingsSubmenu extends FormApplication {
  constructor() {
    super({});
  }
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      closeOnSubmit: false,
      classes: ['form'],
      popOut: true,
      width: "550",
      tabs: [{ navSelector: ".door-sounds-navigation.tabs", contentSelector: ".document-tabs" }],
      height: "auto",
      template: `/modules/door-sounds/templates/settings-submenu.hbs`,
      id: 'door-sounds-settings-submenu',
      title: game.i18n.format("DOORSOUNDS.settingsTitle"),
      resizable: false,

    });
  }
  activateListeners(html) {
    super.activateListeners(html);
    const sheet = this;
    const deleteIcon = html.find(".door-sounds-tab-delete");
    deleteIcon.click(function () {
      deleteSettingEntry(this.parentNode.dataset.tab);
      sheet.render(true);
    });
    const picker = html.find(".picker");
    picker.click(function () {
      const button = this;
      new FilePicker({
        type: "audio",
        callback: async function (imagePath) {
          button.nextElementSibling.value = imagePath;
        }
      }).render(true);
    })
  }
  async getData() {
    const source = game.settings.get(MODULE_ID, "collection") ?? [];
    return source;
  }

  async _updateObject(event) {
    const button = event.submitter;
    if (button.name === "save") {
      await saveSettingEntry(event.srcElement, button.dataset.key)
      this.render(true);
    }
    if (button.name === "done") {
      SettingsConfig.reloadConfirm({ world: true });
    }
  }
}

class DoorSoundsImportExportSubmenu extends FormApplication{
  constructor() {
    super({});
  }
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      closeOnSubmit: false,
      classes: ['form'],
      popOut: true,
      width: "250",
      height: "auto",
      template: `/modules/door-sounds/templates/importexport-submenu.hbs`,
      id: 'door-sounds-importexport-submenu',
      title: game.i18n.format("DOORSOUNDS.exportTitle"),
      resizable: false,
    });
  }
  activateListeners() {
    const sheet = this;
    const [html] = sheet.element;
    const [importButton] = html.getElementsByClassName("door-sounds-import");
    const [exportButton] = html.getElementsByClassName("door-sounds-export");
    console.log(html, importButton, exportButton)
    importButton.addEventListener("click", function(){
      importJson()
    })
    exportButton.addEventListener("click", function(){
      exportJson();
    })
  }
  async _updateObject(event) {
    const button = event.submitter;
    if (button.name === "done") {
      SettingsConfig.reloadConfirm({ world: true });
    }
  }
}



export const registerSettings = function () {
  game.settings.registerMenu(MODULE_ID, "allSettings", {
    name: game.i18n.format("DOORSOUNDS.settings"),
    label: game.i18n.format("DOORSOUNDS.settingsButton"),
    hint: game.i18n.format("DOORSOUNDS.settingsHint"),
    icon: 'fas fa-atlas',
    type: DoorSoundsSettingsSubmenu,
    restricted: true
  });
  game.settings.registerMenu(MODULE_ID, "importExport", {
    name: game.i18n.format("DOORSOUNDS.importexport"),
    hint: game.i18n.format("DOORSOUNDS.importexportHint"),
    icon: 'fa-solid fa-download',
    type: DoorSoundsImportExportSubmenu,
    restricted: true
  });
};